using System;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {
        public ActionResult index()
        {
            return Content("Hello, World!  It is " + DateTime.Now.ToString());
        }
    }
}